#!/bin/sh
# NTG MSP Tools Universal Installer

# Usage Examples:
# wget -q -O - https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/raw/master/msp_linux.sh | bash 

# Set Variables Before Main

export masterURL="sm.ntg.co"

################## Functions ########################

root_check() {
  if [[ $USER != "root" ]]; then 
    echo "This script must be run as root"
    exit 1 
  fi
}

## Linux Section  ********************************************

linux_common() {
  mkdir -p /etc/salt/
  echo "master: $masterURL" > /etc/salt/minion 
  uuidgen > /etc/salt/minion_id 
  echo "67.203.9.2 salt" >> /etc/hosts
}

is_fedora() {
  root_check
  dnf -y install salt-minion 
  linux_common
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_rhel() {
  # Have to disable GPG Check for reliability reasons, real world issues with repos
  root_check
  yum -y install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm --nogpgcheck
  yum -y install python-util salt-minion --nogpgcheck
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_deb() {
  root_check
  apt-get -y update 
  apt-get -y install python-psutil salt-minion uuid-runtime
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_suse() {
  root_check
  zypper install -y salt-minion
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_pacman() {
  root_check
  pacman -Sy salt
  linux_common
  systemctl disable salt-master
  systemctl stop salt-master
  systemctl restart salt-minion
  systemctl enable salt-minion 
  exit   
}

is_raspbian() {
  apt-get -y install uuid-runtime
  is_deb
  exit
}

is_unknown_linux() {
  root_check
  echo "Linux Type is Unknown, So Attempting a Brute Force"
  apt-get -y update && apt-get install salt-minion
  dnf -y install salt-minion
  yum -y install salt-minion
  pacman -Sy salt
  linux_common
  systemctl disable salt-master
  systemctl stop salt-master
  systemctl restart salt-minion
  systemctl enable salt-minion 
  echo "Resulting Install Status is Unknow:"
  echo "Please Report the Following to SodiumSuite Support..."
  echo OS is $OS
  echo VER is $VER
  exit   
}

## Windows Section *****************************************************

is_windows() {
  echo "Windows is not supported, use the Windows script"
  exit 1
}


is_osx() {
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew install saltstack
  export PATH="/usr/local/Cellar/saltstack/2017.7.4/libexec/bin/:$PATH" 
  pip2 install psutil
  sudo mkdir -p /etc/salt
  mkdir -p /tmp/etc/salt
  echo $companyID > /tmp/etc/salt/companyName.txt
  echo "master: $masterURL" > /tmp/etc/salt/minion
  uuidgen > /tmp/etc/salt/minion_id
  sudo cp /tmp/etc/salt/* /etc/salt/
  su -c echo "67.203.9.2 salt" >> /etc/hosts
  sudo curl -L https://raw.githubusercontent.com/saltstack/salt/develop/pkg/darwin/com.saltstack.salt.minion.plist -o /Library/LaunchDaemons/com.saltstack.salt.minion.plist
  sudo launchctl load -w /Library/LaunchDaemons/com.saltstack.salt.minion.plist
  exit 
}

## Detection Section **************************************************************

detect_more() {
  OS=$(uname -s)
  case "$OS" in
    OpenBSD*) is_openbsd ;;
    NetBSD*)  is_netbsd ;; 
    *)        echo "Please tell SodiumSuite Support: $OSTYPE : $OS"  ;;
  esac
}

is_linux() {
  if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
  elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
  elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
  elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
  elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    OS=suse
  elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    OS="CentOS Linux"
  elif [ -f /etc/alpine-release ]; then
    OS="Alpine Linux"
  else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
  fi

  if   [[ "$OS" == "Fedora" ]]; then
    is_fedora
  elif [[ "$OS" == "Ubuntu" ]]; then 
    is_deb
  elif [[ "$OS" == "CentOS Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "Oracle Linux Server" ]]; then
    is_rhel 
  elif [[ "$OS" == "Linux Mint" ]]; then
    is_deb
  elif [[ "$OS" == "Deepin" ]]; then
    is_deb
  elif [[ "$OS" == "Debian GNU/Linux" ]]; then
    is_deb
  elif [[ "$OS" == "Sangoma Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "openSUSE Leap" ]]; then
    is_suse
  elif [[ "$OS" == "openSUSE Tumbleweed" ]]; then
    is_suse
  elif [[ "$OS" == "elementary OS" ]]; then
    is_deb
  elif [[ "$OS" == "Korora" ]]; then
    is_fedora
  elif [[ "$OS" == "Zorin OS" ]]; then
    is_deb
  elif [[ "$OS" == "Antergos Linux" ]]; then
    is_pacman
  elif [[ "$OS" == "Manjaro Linux" ]]; then
    is_pacman
  elif [[ "$OS" == "Solus" ]]; then
    is_eopkg
  elif [[ "$OS" == "PCLinuxOS" ]]; then
    is_pclinuxos 
  elif [[ "$OS" == "Sabayon" ]]; then
    is_entropy
  elif [[ "$OS" == "Gentoo" ]]; then
    is_emerge
  elif [[ "$OS" == "KDE neon" ]]; then
    is_deb
  elif [[ "$OS" == "Netrunner" ]]; then
    is_deb
  elif [[ "$OS" == "Raspbian GNU/Linux" ]]; then
    is_raspbian
  elif [[ "$OS" == "Kali GNU/Linux" ]]; then
    is_deb
  elif [[ "$OS" == "Amazon Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "Alpine Linux" ]]; then
    is_alpine
  elif [[ "$OS" == "ClearOS" ]]; then
    is_rhel
  elif [[ "$OS" == "KaOS" ]]; then
    is_kaos
  else
    echo "Unknown type, need a brute forcer. Diagnostics:"
    echo OS is $OS
    echo VER is $VER
  fi
}

check_ostype() {
  if [ -f /etc/alpine-release ]; then
    OSTYPE="linux" #Alpine Linux Doesn't List Linux
  fi

  case "$OSTYPE" in
    solaris*)   is_solaris ;;
    darwin*)    is_osx ;; 
    linux*)     is_linux ;;
    bsd*)       is_bsd ;;
    FreeBSD*)   is_bsd ;;
    DragonFly*) is_dragonfly ;;
    msys*)      is_windows ;;
    *)          detect_more ;;
  esac
}

main() {
  check_ostype
}

main "$@"
