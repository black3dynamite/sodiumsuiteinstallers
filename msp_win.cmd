set masterURL="sm.ntg.co"

@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin\" -y  
choco install saltminion -y --ignore-checksums --force --force-dependencies   
echo master: %masterURL% > c:\salt\conf\minion.d\mast.conf 
echo "67.203.9.2 salt" >> c:\Windows\System32\Drivers\etc\hosts
@powershell [guid]::NewGuid().toString() > c:\salt\conf\minion_id
net stop salt-minion && net start salt-minion
